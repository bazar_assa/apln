# Apln

## Description

Bash absolute path symbolc linker, requires coreutils

## Installation

One-liner, if your shell is bash.

`curl -O https://gitlab.com/bazar_assa/apln/-/raw/main/apln && chmod +x apln && sudo cp apln /usr/local/bin/ && source ~/.bashrc`

## Usage

`apln [RELATIVE PATH TO FILE] [RELATIVE PATH TO SYMBOLIC LINK]`
